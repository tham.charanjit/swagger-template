[![pipeline status](https://gitlab.com/tijsg/swagger-template/badges/master/pipeline.svg)](https://gitlab.com/tijsg/swagger-template/commits/master)
# Automatic documentation
This example is hosted at: [https://tijsg.gitlab.io/swagger-template]
## Usage
* Fork this project and remove the examples and readme or download the .gitlab-ci.yml file
* Go to [http://editor.swagger.io]
* Create the YAML contract for your service
* Save it and add it to your own Gitlab repository
* Commit and push
* Your YAML contracts will be converted to webpage documentation automatically
